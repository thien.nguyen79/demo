package com.example.demo.repository;

import com.example.demo.model.Role;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

@EnableJpaRepositories
public interface RoleRepository extends CrudRepository<Role, Integer> {
}
